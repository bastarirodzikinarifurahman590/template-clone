<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;

use DB;

class pertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }
    public function store(request $request){
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:pertanyaan|max:255',
            'body' => 'required',
        ]);
        
        $query = DB::table('pertanyaan')->insert([
        "title"=>$request["title"],
        "body"=>$request["body"]
         ]);
         return redirect('/pertanyaan');
         
        }
        public function index(){
            $pertanyaan =pertanyaan ::get();
            return view('pertanyaaan.index',compact('pertanyaan'));
            }
            public function show($id){
                $pertanyaan = DB::table ('pertanyaan')->where('id',$id)->first();
                return view('pertanyaaan.show',compact('pertanyaan'));
                }
                public function edit($id){
                    $pertanyaan = DB::table ('pertanyaan')->where('id',$id)->first();
                    return view('pertanyaaan.show',compact('pertanyaan'));
                }
                public function update($id){
                   request ->validate([
                    'title' => 'required|unique:pertanyaan|max:255',
                    'body' => 'required',
                   ]);
                
                $query = DB::table('pertanyaan')
                ->where(('id',$id))
                ->update([
                    "title"=>$request["title"],
                    "body"=>$request["body"]
                     ]);
                     return redirect('/pertanyaan'); 
                }
                public function update($id){
                    $query = DB::table('pertanyaan')
                    ->where(('id',$id))
                    ->update()
                    return redirect('/pertanyaan');
                }
        }
        

