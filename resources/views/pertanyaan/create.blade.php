@extends('adminlte.master')
@section('content')
<div class="ml-3 mt-3" >
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">create new pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan" methode="post">
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="title">
                  </div>
                  <div class="form-group">
                    <label for="body">body</label>
                    <input type="text" class="form-control" id="body" name="body" placeholder="body">
                  </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">create</button>
                </div>
              </form>
            </div>
</div>

@endsection