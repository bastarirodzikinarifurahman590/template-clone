<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/Welcom" method="POST">
        @csrf
        <label>First Name</label><br><br>
        <input type="text" name="fnama" id=nama><br><br>
        <label>Last Name</label><br><br>
        <input type="text" name="lnama" id=nama><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="Gender">Male <br>
        <input type="radio" name="Gender">Female <br>
        <input type="radio" name="Gender">Other<br><br>
        <label >Jenis Kelamin :</label>
        <select name="Nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Australian">Australian</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br><br>
        <input type="checkbox">English <br><br>
        <input type="checkbox">Other <br><br>
        <label>Bio:</label><br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea><br><br>
        <button type="submit" name="submit">Sign Up</button>
    </form>
    </body>
</html>